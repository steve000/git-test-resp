/*
 * @Author: WangChao
 * @Date: 2022-07-20 11:15:11
 * @LastEditors: WangChao
 * @LastEditTime: 2022-07-20 18:32:28
 * @Description: 
 * @FilePath: \ykb_trip_vuef:\cache\vue-form-making\vue.config.js
 * @filePathColon: /
 */
const TerserPlugin = require('terser-webpack-plugin')
const TerserPlugin1 = require('terser-webpack-plugin1')

module.exports = {
  productionSourceMap: false,
  publicPath: './',
  configureWebpack: config => {
    let plugins = [
      new TerserPlugin({
        terserOptions: {
          compress: {
            aaa: false,
            warnings: false,
            drop_debugger: false,
            drop_console: true,
          },
        },
        sourceMap: false,
        parallel: true,
      })
    ]
    if (process.env.NODE_ENV !== 'development') {
      config.plugins = [...config.plugins, ...plugins]
    }
  }
}